const mongoose = require('mongoose')

const courseSchema = new mongoose.Schema({

	name:{
		type: String,
		required: [true, 'Name is required']
	},
	
	discription:{
		type:String,
		required:[true, 'Description is required']
	},
	
	price:{
		type:Number,
		required:[true, 'Price is required']
	},	
	
	isActive:{
		type:Boolean,
		default: true
	},
	createdOn:{
		type:Date,
		required: new Date()
	},
	
	enrollees:[
		{
			userId: {
				type: String,
				required: [true, 'User ID is required']
			},

			enrolledOn:{
				type:Date,
				default: new Date()
			}
		}
	]
});

module.exports = mongoose.model('Course', courseSchemas)
